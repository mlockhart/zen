#!/usr/bin/env ruby

#gem install mini_magick for the gem that does the work
require 'mini_magick'

def negate_images(files)
    files.each do |file|
        begin
            image = MiniMagick::Image.open(file)
            negated_image = image.negate
            new_file = File.basename(file, ".*") + "_negated" + File.extname(file)
            negated_image.write(new_file)
            puts "negated #{file} to #{new_file}"
        rescue StandardError => e
            puts "Error processing #{file}: #{e.message}"
        end
    end
end

# Read files from command-line arguments (skip the script name)
files = ARGV[1..]

if files.empty?
    puts "Please provide a list of image files to negate."
else
    negate_images(files)
end
