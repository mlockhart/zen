#!/usr/bin/env bash
echo
if [[ $(hostname) =~ mjl ]]; then
  # Opinionated dotfiles and bash library - only load for my own containers
  curl -L https://milohax-net.gitlab.io/radix/bootstrap/profile | sh
  ~/lib/bash/bin/dude
  git config --global user.email mlockhart@gitlab.com
fi

if ! test -d ~/.ssh; then
  mkdir ~/.ssh
  chmod 700 ~/.ssh
fi
cp /workspaces/zen/.devcontainer/config.ssh ~/.ssh/config

if ! type negate; then
  gem install mini_magick
  ln -s /workspaces/zen/.devcontainer/negate.rb ~/bin/negate
fi

if ! type greenhat; then
  sudo gem update --system
  gem install greenhat
fi

test -d ~/lab || mkdir -p ~/lab
pushd ~/lab

# Debian-packaged rust is too old for GitLab Support tools,
# so install the latest Rust with rustup
if ! type cargo; then
  curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs > rustup.sh
  chmod +x rustup.sh
  sh rustup.sh -y
  rm -f rustup.sh
fi
source $CARGO_HOME/env

# Clone Support tools for use in this container
# Note: DotFiles is also useful, but not for SOS generally, and needs maOS
for TOOL in \
  fast-stats \
  gitlabrb_sanitizer \
  zd-dl-wiper \
  strace-parser
do
  if [ -d ${TOOL} ]; then
    pushd ${TOOL}
    git pull
    popd
  else
    git config --global \
      --add safe.directory ${PWD}/${TOOL}
    git clone https://gitlab.com/gitlab-com/support/toolbox/${TOOL}.git
  fi
done

type fast-stats || cargo install --path fast-stats
type strace-parser || cargo install --path strace-parser
gitlab-structlog || go install gitlab.com/gitlab-com/support/toolbox/gitlab-structlog@latest

popd

pushd ~/lab/zd-dl-wiper
  mkdir ~/Downloads
  ln -s /workspaces/zen ~/Downloads/zen
  bundle install
popd

