# Zen - toolbox for working on GitLab Support SOS within a container

This provisions a bare-bones Ruby (which is based on Debian) and installs some [GitLab Support tools](https://gitlab.com/gitlab-com/support/toolbox/), as well as [my dotfiles](https://gitlab.com/milohax-net/radix/dotfiles) and [bash scripts](https://gitlab.com/milohax-net/radix/bashing) to make using [greenhat](https://gitlab.com/gitlab-com/support/toolbox/greenhat)/[fast-stats](https://gitlab.com/gitlab-com/support/toolbox/fast-stats)/[strace-parser](https://gitlab.com/gitlab-com/support/toolbox/strace-parser) and so on easy, and isolated from whatever mess is going on in my workstation.

This way, I can have a known-working toolbox for supporting customers, within a few minutes, without worrying that I've messed up some ruby gems or have the wrong version of Rust, or a weird Homebrew thing, and so on.

## Usage

Install this in the location where you will [download Zendesk attachments](https://gitlab.com/gitlab-com/support/toolbox/zd-dl-router/
) (usually `~/Downloads/zendesk`, or I use `~/Downloads/zen`) and then open the directory in a [VSCode Dev Container](https://code.visualstudio.com/docs/devcontainers/containers) with a container tool running (I've used [SUSE Rancher Desktop](https://rancherdesktop.io/) successfully on Apple Macintosh hosts). It will start `install-toolbox.sh` to provisioning a container and install tools.

In a hurry? You can use greenhat and the other tools _as soon as they install, you_ don't _need to wait for the entire Configuration to finish_.

There are VSCode Tasks to run the `install-dotfiles` and `install-bashing.sh` scripts, for my own creature comforts, but these will not install from the bootstrap `install-toolox.sh`, so you'll only get my quirks if you ask for them ;-)
